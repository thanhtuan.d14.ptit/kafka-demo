package demo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.configs.KafkaConfig;
import demo.models.Message;
import demo.models.Request;
import demo.producers.SendToTopic;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;
import java.util.UUID;

@Slf4j
public class TestService {

    private SendToTopic sendToTopic;

    @Before
    public void setupEnv() {
        ObjectMapper objectMapper = new ObjectMapper();
        Properties propertiesProducer = KafkaConfig.configProducer();
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(propertiesProducer);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        this.sendToTopic = new SendToTopic(objectMapper, kafkaProducer);

        KafkaConfig.createNewTopic(Constant.TOPIC_TEST_REQUEST, 4, 3);
        KafkaConfig.createNewTopic(Constant.TOPIC_TEST_RESPONSE, 4, 3);

    }

    @Test
    public void testSendMessage(){

        for(int i=0;i<10;i++) {
            Message message = new Message();
            Request request = new Request();
            request.setName("Test message: "+i);
            message.setSocketId("0");
            message.setRequest(request);
            message.setKey(UUID.randomUUID().toString());

            RecordMetadata recordMetadata = sendToTopic.sendMessage(Constant.TOPIC_TEST_REQUEST, message);

            assert recordMetadata != null;
        }
    }

}
