var ws;

function connect() {

    var host = document.location.host;
    var pathname = document.location.pathname;

    ws = new WebSocket("ws://" +host  + pathname + "message");

    ws.onmessage = function(event) {
        var log = document.getElementById("log");

        try {
            var res = JSON.parse(event.data).request;
            var str = "TIME: "+res.createdDate+", "+res.name+", DESCRIPTION: "+res.description;
            log.innerHTML += str + "\n";
        }catch (e) {
            log.innerHTML += event.data +"\n";
        }

    };
}

function send() {
    var content = document.getElementById("msg").value;
    var loop = parseInt(document.getElementById("loop").value);

    for(var i=0;i<loop;i++){
        var json = JSON.stringify({
            name:content,
            age: 20,
            id:Math.floor(Math.random() * Math.floor(10000))
        });
        ws.send(json);
    }
}