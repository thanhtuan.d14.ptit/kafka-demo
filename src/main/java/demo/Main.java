package demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.configs.KafkaConfig;
import demo.consumers.ThreadTopic;
import demo.producers.SendToTopic;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;

@Slf4j
public class Main {

    public static void main(String[] args) {

        ObjectMapper objectMapper = new ObjectMapper();

        KafkaProducer<String, String> kafkaProducer = KafkaConfig.connectProducer();
        KafkaConsumer<String, String> a = KafkaConfig.connectConsumer();

        ThreadTopic topic = new ThreadTopic(a,
                objectMapper,
                new SendToTopic(objectMapper, kafkaProducer),
                null,
                Constant.TOPIC_TEST_REQUEST,
                Constant.TOPIC_TEST_RESPONSE);
        topic.start();
    }
}
