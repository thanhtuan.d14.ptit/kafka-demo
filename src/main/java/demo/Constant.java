package demo;

public interface Constant {
    String DEFAULT_TOPIC_CONSUMER = "DEFAULT_TOPIC_CONSUMER_1";

    String RESPONSE_TOPIC = "RESPONSE_TOPIC_1";

    String TOPIC_TEST_REQUEST = "agent-request";

    String TOPIC_TEST_RESPONSE = "agent-response";

    String DATE_TIME_PATTERN = "HH:mm:ss dd/MM/yyyy";
}
