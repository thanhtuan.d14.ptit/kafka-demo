package demo.consumers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.models.Message;
import demo.producers.SendToTopic;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import javax.websocket.Session;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class ThreadTopic extends Thread {

    private final KafkaConsumer<String, String> kafkaConsumer;

    private final ObjectMapper objectMapper;

    private final SendToTopic sendToTopic;

    private final Session session;

    private LinkedList<String> keyMessage;

    private final String topicRequest;
    
    private final String topicResponse;

    private final ExecutorService executorService;

    public ThreadTopic(KafkaConsumer<String, String> kafkaConsumer,
                       ObjectMapper objectMapper,
                       SendToTopic sendToTopic, Session session,
                       String topicRequest,
                       String topicResponse){

        this.kafkaConsumer = kafkaConsumer;
        this.objectMapper = objectMapper;
        this.sendToTopic = sendToTopic;
        this.session = session;
        this.topicRequest = topicRequest;
        this.topicResponse = topicResponse;
        this.keyMessage = new LinkedList<>();

        executorService = Executors.newFixedThreadPool(10);

        this.kafkaConsumer.subscribe(Arrays.asList(this.topicRequest, this.topicResponse));
    }

    public void addKeyMessage(String keyMessage) {
        this.keyMessage.add(keyMessage);
    }

    @Override
    public void run() {


        while (true) {
            ConsumerRecords<String, String> consumerRecords;
            consumerRecords = kafkaConsumer.poll(Duration.ofSeconds(3));
//            log.info("POOL PARTITION: {}, COUNT: {}",consumerRecords.partitions(),consumerRecords.count());
            //Listen by partition
//            consumerRecords.records(new TopicPartition(topicRequest, 0))
//                    .forEach(this::listenMessageTopicRequest);
//
//            consumerRecords.records(new TopicPartition(topicRequest, 1))
//                    .forEach(this::listenMessageTopicRequest1);
//
//            consumerRecords.records(new TopicPartition(topicRequest, 2))
//                    .forEach(this::listenMessageTopicRequest2);
//
//            consumerRecords.records(new TopicPartition(topicRequest, 3))
//                    .forEach(this::listenMessageTopicRequest3);
//
//            consumerRecords.records(new TopicPartition(topicResponse, 0))
//                    .forEach(this::listenMessageTopicResponse);
//
//            consumerRecords.records(new TopicPartition(topicResponse, 1))
//                    .forEach(this::listenMessageTopicResponse1);
//
//            consumerRecords.records(new TopicPartition(topicResponse, 2))
//                    .forEach(this::listenMessageTopicResponse2);
//
//            consumerRecords.records(new TopicPartition(topicResponse, 3))
//                    .forEach(this::listenMessageTopicResponse3);


            //Listen by topic
            consumerRecords.records(topicRequest)
                    .forEach(this::listenMessageTopicRequest);

            consumerRecords.records(topicResponse)
                    .forEach(this::listenMessageTopicResponse);
        }

    }

    private void formListenRequest(ConsumerRecord<String, String> record){
        Thread a = new Thread(()->{
            log.info("message from request : {}", record.value());

            try {
                JsonNode jsonNode = objectMapper.readTree(record.value());
                Message value = objectMapper.convertValue(jsonNode, Message.class);
                String name = ("FULL NAME : ").concat(value.getRequest().getName().toUpperCase());

                value.getRequest().setName(name);

                value.getRequest().setDescription("From topic: "+record.topic()+", partition: "+record.partition());

                sendToTopic.sendMessage(topicResponse, value);

            } catch (IOException | IllegalArgumentException e) {
                log.error("Error listen topic request ",e);
            }
        });

        executorService.execute(a);
    }

    private void formListenResponse(ConsumerRecord<String, String> record){

        Thread a = new Thread(()->{
            log.info("MESSAGE_RESPONSE: {}", record.value());

            //TODO for running websocket
            try {
                JsonNode jsonNode = objectMapper.readTree(record.value());
                Message message = objectMapper.convertValue(jsonNode, Message.class);
                if (
                        session.getId().equals(message.getSocketId())
                                && keyMessage.contains(message.getKey())
                ) {
                    keyMessage.remove(message.getKey());
                    session.getBasicRemote().sendText(record.value());
                }
            } catch (IOException e) {
                log.error("Error when send response socket !", e);
            }
            //TODO for running websocket
        });

        executorService.execute(a);

    }

    private void listenMessageTopicResponse(ConsumerRecord<String, String> record){
        formListenResponse(record);
    }

    private void listenMessageTopicResponse1(ConsumerRecord<String, String> record){
        formListenResponse(record);
    }

    private void listenMessageTopicResponse2(ConsumerRecord<String, String> record){
        formListenResponse(record);
    }

    private void listenMessageTopicResponse3(ConsumerRecord<String, String> record){
        formListenResponse(record);
    }

    private void listenMessageTopicRequest(ConsumerRecord<String, String> record){
        formListenRequest(record);
    }

    private void listenMessageTopicRequest1(ConsumerRecord<String, String> record){
        formListenRequest(record);
    }

    private void listenMessageTopicRequest2(ConsumerRecord<String, String> record){
        formListenRequest(record);
    }

    private void listenMessageTopicRequest3(ConsumerRecord<String, String> record){
        formListenRequest(record);
    }


}
