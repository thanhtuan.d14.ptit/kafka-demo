package demo.configs;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Properties;

@Slf4j
public class KafkaConfig {

    public static KafkaProducer<String, String> connectProducer(){
        return new KafkaProducer<>(configProducer());
    }

    public static KafkaConsumer<String, String> connectConsumer(){
        return new KafkaConsumer<>(configConsumer());
    }

    public static Properties configConsumer(){
        Properties configProperties = new Properties();
        configProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.2.16.182:9092");
        configProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        configProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        configProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "VTPay");
        configProperties.put(ConsumerConfig.CLIENT_ID_CONFIG, "tuanbht_consumer_0");
        configProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
//        configProperties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1000);
        configProperties.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 5*60*1000);
//        configProperties.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG,1*60*1000);
        return configProperties;
    }

    public static Properties configProducer(){
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"10.2.16.182:9092");
        configProperties.put(ProducerConfig.CLIENT_ID_CONFIG,"tuanbht_producer");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        return configProperties;
    }

    public static void createNewTopic(String topic, int partition, int replica){
        String zookeeperHost = "10.2.16.182:2181";
        boolean isSecure = false;
        int sessionTimeoutMs = 5*60*1000;
        int connectionTimeoutMs = 1*60*1000;
        int maxInFlightRequests = 10;
//        Time time = Time.SYSTEM;
//        String metricGroup = "myGroup";
//        String metricType = "myType";
//        Option<String> name = Option.apply("zkClient");
//
//        KafkaZkClient zkClient = KafkaZkClient.apply(zookeeperHost,
//                isSecure,
//                sessionTimeoutMs,
//                connectionTimeoutMs,
//                maxInFlightRequests,
//                time,
//                metricGroup,
//                metricType,
//                name);
//
//        Properties properties = new Properties();
//
//        AdminZkClient adminZkClient = new AdminZkClient(zkClient);
//
//        try {
//            adminZkClient.createTopic(topic, partition, replica, properties, RackAwareMode.Disabled$.MODULE$);
//
//        }catch (Exception e){
//            log.warn("Topic {} already exists !",topic);
//        }

    }

}
