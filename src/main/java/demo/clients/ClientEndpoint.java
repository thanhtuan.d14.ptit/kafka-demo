package demo.clients;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.Constant;
import demo.configs.KafkaConfig;
import demo.consumers.ThreadTopic;
import demo.models.Message;
import demo.models.Request;
import demo.producers.SendToTopic;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@ServerEndpoint("/message")
@Slf4j
public class ClientEndpoint {

    private final KafkaConsumer<String, String> kafkaConsumer;

    private final ObjectMapper objectMapper;

    private ThreadTopic threadTopic;

    private final SendToTopic sendToTopic;

    public ClientEndpoint(){
        log.info("Starting websocket endpoint : /message");
        this.kafkaConsumer = KafkaConfig.connectConsumer();

        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        this.sendToTopic = new SendToTopic(objectMapper, KafkaConfig.connectProducer());

        KafkaConfig.createNewTopic(Constant.DEFAULT_TOPIC_CONSUMER, 4, 1);
        KafkaConfig.createNewTopic(Constant.RESPONSE_TOPIC, 4, 1);
    }

    @OnOpen
    public void onOpen(Session session) {
        log.info("Session opened, id: {}", session.getId());
        threadTopic = new ThreadTopic(kafkaConsumer,
                objectMapper,
                sendToTopic,
                session,
                Constant.DEFAULT_TOPIC_CONSUMER,
                Constant.RESPONSE_TOPIC);
        threadTopic.start();
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("Message received. Session id: {} Message: {}",
                session.getId(), message);
        try {

            JsonNode jsonNode = objectMapper.readTree(message);

            Request request = new Request(
                    jsonNode.get("id").asInt(),
                    jsonNode.get("name").asText(),
                    jsonNode.get("age").asInt());

            Message messageParsed = new Message();
            messageParsed.setSocketId(session.getId());
            messageParsed.setRequest(request);
            messageParsed.setKey(UUID.randomUUID().toString());
            threadTopic.addKeyMessage(messageParsed.getKey());

            sendToTopic.sendMessage(Constant.DEFAULT_TOPIC_CONSUMER, messageParsed);

        } catch (IOException ex) {
            threadTopic.interrupt();
            session.getAsyncRemote().sendText("{name : \"HAS ERROR\"}");
        }
    }

    @OnError
    public void onError(Session session, Throwable e) {
        threadTopic.interrupt();
        session.getAsyncRemote().sendText("{request: {name : \"HAS ERROR\", " +
                "createdDate : \""+ new SimpleDateFormat(Constant.DATE_TIME_PATTERN).format(new Date()) +"\","+
                "description: \""+e.getMessage()+"\"}}");
        e.printStackTrace();
    }

    @OnClose
    public void onClose(Session session) {

        threadTopic.interrupt();

        log.info("Session closed with id: {}", session.getId());
    }
}
