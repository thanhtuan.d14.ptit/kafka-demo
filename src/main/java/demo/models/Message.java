package demo.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String key;

    private String socketId;

    private Request request;

    @Override
    public String toString() {
        return "Message{" +
                "key='" + key + '\'' +
                ", socketId='" + socketId + '\'' +
                ", request=" + request +
                '}';
    }
}
