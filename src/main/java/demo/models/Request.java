package demo.models;

import demo.Constant;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class Request {

    private String description;

    private Integer id;

    private String createdDate;

    private String name;

    private Integer age;


    public Request(Integer id, String name, Integer age){
        this.id = id;
        this.name = name;
        this.age = age;
        this.createdDate = new SimpleDateFormat(Constant.DATE_TIME_PATTERN).format(new Date());
    }

    public Request(){
        this.createdDate = new SimpleDateFormat(Constant.DATE_TIME_PATTERN).format(new Date());
    }

    @Override
    public String toString() {
        return "Request{" +
                "description='" + description + '\'' +
                ", id=" + id +
                ", createdDate='" + createdDate + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
