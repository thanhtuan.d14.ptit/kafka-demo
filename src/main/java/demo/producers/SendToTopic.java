package demo.producers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.models.Message;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
public class SendToTopic {

    private final ObjectMapper objectMapper;

    private final KafkaProducer<String, String> kafkaProducer;

    public SendToTopic(ObjectMapper objectMapper, KafkaProducer<String, String> kafkaProducer){

        this.objectMapper = objectMapper;
        this.kafkaProducer = kafkaProducer;
    }

    public RecordMetadata sendMessage(String topic, Message message){

        try {

            String messageParse = objectMapper.writeValueAsString(message);

            log.info("Sending message to Topic: {}, Message : {}", topic, message);

            ProducerRecord<String, String> record = new ProducerRecord<>(topic, messageParse);

            Future<RecordMetadata> response = kafkaProducer.send(record);

            RecordMetadata res =  response.get(1, TimeUnit.SECONDS);

            log.info("RESPONSE_SEND_TO_TOPIC {} : {}", topic, res);

            return res;
        } catch (InterruptedException | ExecutionException | JsonProcessingException | TimeoutException e) {
            log.error("REQUEST_TIMEOUT: Topic {}, Message {}", topic, message, e);
        }

        return null;
    }

}
